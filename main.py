def one_through_twenty():
    return

#Retornar os números de 1 a 20. (1, 2, 3,…, 19, 20)

def evens_to_twenty():
    return

#Retornar os números pares de 1 a 20. (2, 4, 6,…, 18, 20)

def odds_to_twenty():
    return

#Retornar os números ímpares de 1 a 20. (1, 3, 5,…, 17, 19)

def multiples_of_five():
    return

#Retornar os múltiplos de 5 até 100. (5, 10, 15,…, 95, 100)

def square_numbers():
    return

#Retornar todos os números até 100 que forem quadrados perfeitos. (1, 4, 9, …, 81, 100)

def counting_backwards():
    return

#Retornar os números contando de trás para frente de 20 até 1. (20, 19, 18, …, 2, 1)

def even_numbers_backwards():
    return

#Retornar os números pares de 20 até 1. (20, 18, 16, …, 4, 2)

def odd_numbers_backwards():
    return

#Retornar os números ímpares de 20 até 1. (19, 17, 15, …, 3, 1)

def multiple_of_five_backwards():
    return

#Retornar os múltiplos de 5 contando de trás para frente a partir de 100. (100, 95, 90, …, 10, 5)

def square_numbers_backwards():
    return

#Retornar os quadrados perfeitos contando de trás para frente a partir de 100. (100, 81, 64, …, 4, 1)
